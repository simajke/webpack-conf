const path                   = require('path');

/* Plugin to create .html file with all generated scripts in it starts */
const HtmlWebpackPlugin      = require("html-webpack-plugin");
/* Plugin to create .html file with all generated scripts in it ends */

/* Plugin to extract separate css file start */
const MiniCssExtractPlugin   = require("mini-css-extract-plugin");
/* Plugin to extract separate css file end */

/* Autoreload start */
const BrowserSyncPlugin      = require('browser-sync-webpack-plugin');
/* Autoreload end */

/* Cleaning destination folder each time rebuilding an application start*/
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
/* Cleaning destination folder each time rebuilding an application end */

/* Minify image size start */
const ImageminPlugin         = require('imagemin-webpack-plugin').default
const imageminMozjpeg        = require( 'imagemin-mozjpeg');
/* Minify image size end */

/* minify js start */
const TerserPlugin = require("terser-webpack-plugin");
/* minify js end */

/* Minify css start */
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const cssnano = require("cssnano");
/* Minify css end */

const NODE_ENV   = process.env.NODE_ENV || 'production';
const MAP_TYPE   =  process.env.NODE_ENV === 'development' ? 'default' : 'source-map';
// const MAP_TYPE   = 'source-map';
module.exports = () => {
    return {
        mode: NODE_ENV,
        devtool: MAP_TYPE,
        entry: {
            main: path.resolve(__dirname, "src/assets/js/main.js"),
            new:  path.resolve(__dirname, "src/assets/js/new.js")
        },
        output: {
            filename: 'js/[name].[contenthash].bundle.js', // chunks should use [contenthash]
            chunkFilename: 'js/[name].[contenthash].bundle.js',
            publicPath: 'assets/', //the slash at the end is essential ( lazy loading )
            path: path.resolve(__dirname, 'assets')
        },
        watch: true,
        watchOptions: {
            ignored: /node_modules/
        },
        recordsPath: path.join(__dirname, "records.json"),
        optimization: {
            minimize: NODE_ENV === 'production',
            minimizer: [new TerserPlugin({ sourceMap: true })],
            splitChunks: {
                chunks: "initial"
            },
            runtimeChunk: {
                name: "manifest"
            }
        },
        module: {
            rules: [
                {
                    //images and fonts should use [hash] in names
                    test: /\.(jpe?g|png|gif|svg)$/i,
                    use: [
                        {
                            loader: 'url-loader',
                            options: {
                                outputPath: 'images',
                                publicPath: 'assets/images',
                                limit: 25000
                            }
                        }
                    ]
                },
                {
                    test: /\.js$/,
                    exclude: /(node_modules|bower_components)/,
                    use: {
                        loader: "babel-loader",
                        options: {
                            presets: [
                                [
                                    "@babel/preset-env", {
                                        modules: false,
                                        useBuiltIns: "usage",
                                        corejs: 3,
                                    }

                                ]
                            ],
                            plugins: ["@babel/plugin-syntax-dynamic-import"]
                        }
                    }
                },
                {
                    test: /\.css$/,
                    use: [
                        MiniCssExtractPlugin.loader,
                        "css-loader",
                        {
                            loader: "postcss-loader",
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    require("autoprefixer")({}),
                                    require("precss"),
                                ]
                            },
                        },
                        "sass-loader",
                    ]
                }
            ]
        },
        plugins: [
            new BrowserSyncPlugin({
                host: 'localhost',
                port: 8080,
                proxy: "http://webpack-test.com:80/main.html"
            }),
            new MiniCssExtractPlugin({
                filename: "css/[name].[contenthash].css"
            }),
            new OptimizeCSSAssetsPlugin({
                cssProcessor: cssnano,
                cssProcessorOptions: {
                    discardComments: {
                        removeAll: true,
                    },
                    safe: true,
                },
                canPrint: false,
            }),
            new ImageminPlugin({
                test: /\.(jpe?g|png|gif|svg)$/i,
                maxFileSize: 9000000,
                disable: NODE_ENV === 'development',
                jpegtran: null,
                plugins: [
                    imageminMozjpeg({
                        quality: 75,
                        progressive: true
                    })
                ]
            }),
            new CleanWebpackPlugin({}),
            new HtmlWebpackPlugin({
                title: "Main page",
                scriptLoading: "defer",
                inject: true,
                filename: "../main.html",
                template: "./src/assets/html/main.html",
                chunks: ['main']
            })
        ],
    }
};