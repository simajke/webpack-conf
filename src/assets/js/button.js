export default ( text = "Hello world!" ) => {
    const el = document.createElement('div');

    el.className = "pure-button";
    el.innerHTML = text;

    el.onclick = () => {
        import( './lazy' )
            .then( lazy => { el.textContent = lazy.default })
            .catch( err => { console.log( err ) });
    }

    return el;
}